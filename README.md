# Replit-vm

Root-Instance inside of replit's docker container with Proot.

## ✨ Features

- Root well, inside the docker container.
- idk whatelse

## 💁‍♀️ How to use

- first download the sh file in this repo
- make a bash server in replit 
- upload the file and start the server
- it will install everything for u
- now everything is done!

## ✨ Note

- This script is for educational purposes, we are not responsible for anything that happens.
- it is preferred to use replit hackerplan so you can keep the vm 24/7 and will have better resources and disk space.
